<?php session_start();
    include_once("util.php");
    $digits="";
    
    //Validate data:
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    { 
        $digits = test_input($_POST["digits"]);
    }
    if($digits == "4 8 15 16 23 42")
    {
        agregarBitacora("SUCCESS", $_POST["digits"]);
    }
    else if($digits != "4 8 15 16 23 42")
    {
        agregarBitacora("SYSTEM FAILURE", $_POST["digits"]);
    }
    header("Location: index.php");
?>