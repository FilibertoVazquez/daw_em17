<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include("_head.html");?>
    </head>
    <body>
        <br><br><br><br><br>
        <div class="ui center aligned basic container">
            <h1 class="ui header">registros</h1>
            <table class="ui sortable celled striped table">
                <thead>
                    <tr><th>Estado</th><th>Fecha y Hora</th></tr>
                </thead>
                    <?php include("cargaAcciones.php");?>
                </tbody>
            </table>
        </div>
        <br>
    </body>
       <!-- Scripts -->
      <?php include("_scripts.html")?>
</html>