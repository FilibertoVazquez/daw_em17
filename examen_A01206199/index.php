<?php session_start();
  include_once("util.php");
 
  $inactivo = 6480;
  $_SESSION['tiempo']=time();
  if(isset($_SESSION['tiempo']) ) {
    $vida_session = time() - $_SESSION['tiempo'];
    if($vida_session > $inactivo)
    {
      agregarBitacora("SYSTEM FAILURE", "");
      header("Location: index.php"); 
    }
  }

$_SESSION['tiempo'] = time();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include("_head.html");?>
  </head>
  
  <body><br><br><br><br><br><br><br><br><br><br><br><br>
    <div class="ui center aligned grid">
      <div class="column">
        <h2 class="ui header">
          Escribe los digitos:
        </h2>
        <form class="ui large form" action="valida.php" method="post" id="form1" onkeyup="sendRequest()">
          <div id="ajaxResponse"></div>
            <div class="ui stacked segment">
              <div class="field">
                <div class="ui left icon input">
                  <i class="privacy icon"></i>
                  <input type="text" name="digits">
                </div>
              </div>
            <input class="ui fluid large submit button" type="submit" placeholder="Entrar" value="Submit">
            </div>
          <div class="ui error message"></div>
        </form>
        <div class="ui stacked segment">
          <a class="ui basic button" href="#" onClick="cargarRegistros();">Registros</a>
            <a class="ui basic button" href="#" onClick="cargarFailures();">SYSTEM FAILURES</a>
            <a class="ui basic button" href="#" onClick="cargarTodos();">Total de SYSTEM FAILURES y SUCCESS</a>
   
    </div>
      </div>
    </div>
     <div id="reportes">
    
   </div>

<!-- Scripts -->
      <?php include("_scripts.html");?>
      <script type="text/javascript" src="/js/login.js"></script>
      <script> 
        function cargarRegistros(){
            var url="displayAll.php"
            $.ajax({   
                type: "POST",
                url:url,
                data:{},
                success: function(datos){       
                    $('#reportes').html(datos);
                }
            });
        }
        function cargarFailures(){
            var url="displayFailures.php"
            $.ajax({   
                type: "POST",
                url:url,
                data:{},
                success: function(datos){       
                    $('#reportes').html(datos);
                }
            });
        }
        function cargarTodos(){
            var url="displayAmount.php"
            $.ajax({   
                type: "POST",
                url:url,
                data:{},
                success: function(datos){       
                    $('#reportes').html(datos);
                }
            });
        }
     </script>
  <script type="text/javascript" src="/js/login.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>
  </body>
</html>