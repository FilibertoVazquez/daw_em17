<?php 
    
    function connectDb()
    {
        $servername = "localhost";
        $username = "filyv";
        $password = "";
        $dbname = "examen_fily";
        
        
        $conn = mysqli_connect($servername, $username, $password,$dbname);
        
        if(!$conn)
        {
            die("Connection failed: ".mysqli_connect_error);
        }
        
        mysqli_set_charset($conn,"utf8");
        
        return $conn;
    }
    
    function closeDb($mysql)
    {
        mysqli_close($mysql);
    }
    
   function getBitacora()
   {
        $conn=connectDb();
        mysqli_set_charset($conn,"utf8");
        $sql="SELECT Descripcion, Fecha FROM Bitacora ORDER BY Fecha DESC";
        $result=mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
   }
   function getFailure()
   {
        $conn=connectDb();
        mysqli_set_charset($conn,"utf8");
        $sql="SELECT Fecha FROM Bitacora WHERE Descripcion = \"SYSTEM FAILURE\" ORDER BY Fecha DESC";
        $result=mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
   }
   function getSuccess()
   {
        $conn=connectDb();
        
        mysqli_set_charset($conn,"utf8");
        
        $sql="SELECT Fecha FROM Bitacora WHERE Descripcion = \"SUCCESS\" ORDER BY Fecha DESC";
        
        $result=mysqli_query($conn,$sql);
        
        closeDb($conn);
        
        return $result;
   }
   
   function agregarBitacora($desc, $input)
   {
       $conn = connectDB();
       $sql="INSERT INTO Bitacora (Descripcion,Fecha, Evento) VALUES (\"".$desc."\",NOW(), \"".$input."\");";
       mysqli_query($conn,$sql);
       closeDb($conn);
       return $sql;
   }
   
   function totalSuccess()
   {
       $conn = connectDB();
       $sql="SELECT COUNT(Descripcion) FROM Bitacora WHERE Descripcion = \"SUCCESS\"";
       $result=mysqli_query($conn,$sql);
       closeDb($conn);
       return $result;
   }
   function totalFailure()
   {
       $conn = connectDB();
       $sql="SELECT COUNT(Descripcion) FROM Bitacora WHERE Descripcion = \"SYSTEM FAILURE\"";
       mysqli_query($conn,$sql);
       closeDb($conn);
       return $sql;
   }
?>
